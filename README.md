## Developer information

 - compiling DocSearcher  : "mvn clean package" from the current directory should do the job

 - executing DocSearcher  : "mvn exec:java"

 - Changes to the source? : core DocSearcher source files are found in src directory

 - project reports        : "mvn site"

## Questions?
Use the project forum https://sourceforge.net/p/docsearcher/discussion/
