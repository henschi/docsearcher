# Changelog
All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.1.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

### Changed
- use version string from maven
- update maven plugins
- update Lucene to 9.12.1 (supports Java 23)
- update POI to 5.4.0
- update PDFBox to 3.0.3
- update JUnit to 5.11.4
- update Log4j to 2.24.3
- update commons-io to 2.18.0
- update commons-compress to 1.27.1
- update commons-lang3 to 3.17.0

### Security
- add OWASP suppression for false positive detection of CVE-2024-45772

## [4.0.0] - 2024-03-07

### Changed
- important: Java 11 is now the minimum required version
- important: change working directory to .docsearcher2
- important: move working directory under windows to user home directory
- activate antialiasing on linux
- update Lucene to 9.9.2 (api changes)
- update PDFBox to 3.0.1 (api changes)
- update POI to 5.2.5
- update JUnit to 5.10.1
- update Log4j to 2.22.1
- update commons-io to 2.15.1
- update commons-lang3 to 3.14.0
- update Lucene to 8.11.2
- replace FindBugs with SpotBugs
- replace JUnit4 with JUnit5
- update Lucene to 8.8.1
- update Lucene to 8.6.3 (api changes)
- update Lucene to 7.7.3
- update Lucene to 6.6.6
- update Lucene to 5.5.5 (api changes)
- update Lucene to 4.10.4 (api changes)
- update Jacoco to 0.8.6
- remove old md5 library and use commons-codec
- update servlet api to 2.5
- update mail api to 1.6.2
- maven site enhanced
- maven is the build system now

### Fixed
- fix internal text and html view problems
- fix saving index list after delete last index

### Removed
- drop Java 8 Support
- problem fixed during start external application and the filepath starts with more than one /
- removed additional search option (group, phrase), use " in search field to create group
- old web.xml removed, because we build DocSearch.war no longer
- remove ant and libs

### Security
- update commons-compress to 1.26.0 (CVE-2024-25710, CVE-2024-26308)
- don't execute exe and sh files
- update POI to 5.2.2 (CVE-2021-44228, CVE-2021-45046, CVE-2021-45105)
- update PDFBox to 2.0.25 (CVE-2021-31811)
- update Log4j to 2.17.1 (CVE-2021-44832)
- update PDFBox to 2.0.23 (CVE-2021-27807, CVE-2021-27906 Infinite loop and OutOfMemory)
- update POI to 4.1.2 (CVE-2019-12415 - XML External Entity (XXE) Processing in Apache POI versions prior to 4.1.1)


## [3.95.3] - 2024-02-03

### Fixed
- problem fixed during start external application and the filepath starts with more than one / (backport)
- activate antialiasing on linux (backport)


## [3.95.2] - 2020-11-15

### Fixed
- fix saving index list after delete last index (backport)


## [3.95.1] - 2020-07-17

### Fixed
- fix missing create of working directory on first start (backport)


## [3.95.0] - 2020-06-30

### Changed
- update POI to 3.17

### Security
- update PDFBox & FontBox to 1.8.15 (CVE-2018-8036 - DoS (OOM) Vulnerability in Apache PDFBox's AFMParser)


## [3.94.0] - 2018-11-08

### Changed
- add possibility for localized start page (contributed by Olivier Descout <odescout at users dot sf dot net>)
- move DocSearcher to Java 8
- update commons-lang to 3.6
- fix OpenDocument token problem (contributed by Olivier Descout <odescout at users dot sf dot net>)
- change Lucene IndexReader to work read only, because Lucene 4 will skip write support on IndexReader
- update Lucene to 3.6.2 (api changes)
- update Lucene to 3.5.0 (api changes)
- update Lucene to 3.2.0/3.3.0/3.4.0 (no api changes)
- update Lucene to 3.1.0 (api changes)
- update Lucene to 3.0.0 (no api changes)
- update Lucene to 2.9.4 (api changes)
- update Lucene to 2.4.1 (api changes)
- update Lucene to 2.3.2 (no api changes, but Lucene 2.1 changed the index format after index saving)
- add support for OpenDocument spreadsheet, presentation and drawing files (.ods, .ots, .odp, .otp, .odg, .otg) (contributed by Olivier Descout <odescout at users dot sf dot net>)
- add possible file extensions for OpenDocument document files (.odm, .ott), text files (csv, .java, .py, .rst, .md), HTML files (.mhtml .mht, .xhtml, .xhtm), Word files (.dot, .dotx, .docm, .dotm) and Excel files (.xlsm) (contributed by Olivier Descout <odescout at users dot sf dot net>)
- adapt to Checkstyle 6.19, FindBugs 3.0.1 and Cobertura 2.1.1
- fix resource leak in Word, Excel and Powerpoint converter
- add support for Powerpoint 7 files (.ppt, .pps) (contributed by Olivier Descout <odescout at users dot sf dot net>)
- add support for Powerpoint OOXML files (.pptx, .ppsx) (contributed by Olivier Descout <odescout at users dot sf dot net>)
- update commons-io to 2.5
- add commons-collection4 4.1 library (required by POI)
- update POI to 3.15 (contributed by Olivier Descout <odescout at users dot sf dot net>)
- add commons-logging library (required by PDFBox & FontBox)
- update PDFBox & FontBox to 1.8.12 (contributed by Olivier Descout <odescout at users dot sf dot net>)
- update Log4j to 1.2.17 (contributed by Olivier Descout <odescout at users dot sf dot net>)
- use single app version numbering (instead of two) (contributed by Olivier Descout <odescout at users dot sf dot net>)
- move DocSearcher to Java 7
- remove unused help page
- fix problem with set page in main view
- add support for Excel OOXML files (.xlsx)
- add support for Word OOXML files (.docx)
- add support for Word 6/95 files
- update POI to 3.8

### Removed
- drop Java 7 Support
- drop Java 6 Support


## [3.93.0] - 2012-11-07

### Changed
- move DocSearcher to Java 6
- fix problem with ignored files if you searched with filetype choise
- fix upper case file type problem since version 3.92.0
- fix some problems in website spider
- preparing to Java Webstart
- images and icons moved to DocSearch jar


## [3.92.0]

### Changed
- refactored PDF converter
- removed old multivalent PDF extractor
- updated PDFBox to 0.7.3
- changed the Lucene date to new format (DateTools)
- refactored internal filetype handling


## [3.91.0]

### Changed
- no changes


## [3.91.0-rc1]

### Changed
- refactor DocType handling
- refactor index creating and don't store the body content
- search in body and title together is possible again
- replace old setting file "docSearch_prefs.txt" with "docsearcher.properties"
- remove tinylaf layout
- first try to solve some problem with servlet extension
- remove check if last search text was the same, because the options can be changed
- remove option for search in title and body, because it does not run
- fix escaping problem in metadata report
- fix problem with filenames contains whitespaces
- update POI to 3.2 final
- fix some problems in Word and Excel converter
- add commons-io


## [3.90.0]

### Changed
- fix some problems with search field
- fix problem with home button
- add first junit test
- add FindBugs
- add Checkstyle
- ant - move some directories to build
- remove back and forward buttons, because they are not useful in this context
- update Lucene to 2.0.0
- add some Java 5 features
- add new resources
- fix problem with command line


## [3.89]

### Fixed
- little fix in metadata report for OpenDocument
- fix reindex problem with to many filetype variables


## [3.89-pre3]

### Changed
- fix some problems during OpenDocument indexing
- update Jakarta POI library to actual alpha version 2006-05-15
- rewrite Word converter
- rewrite Excel converter
- remove some unused temporary files


## [3.89-pre2]

### Changed
- add important debug for cd creating mechanism
- update Log4j to 1.2.13


## [3.89-pre1]

### Changed
- add filetype constants
- add OpenDocument text format
- fix some different title formats during indexing (filename with path, first content of document)
- document title normally from document or filename
- fix double k(k K bytes) in summary line
- improve filetype handling
- change filetype icons


## [3.88]

### Changed
- no changes since 3.88 pre1


## [3.88-pre1]

### Changed
- fix date convert problem
- many code cleanups
- add license (GPL)
- source code formated (java code convention)
- add Log4j
- little fix in website spider
- add help page
- add Environment and FileEnvironment class
