## TODO for 4.1
- launch4j


## TODO for 4.2
- works even if the index content folder does not exist
  - DocSearch.loadIndexes (mark data not available)
  - DocSearch.doExit (store index)


## TODO for 4.3
- add parser for email files (*.eml)


## TODO for 4.x 
- split source code into individual Maven modules to reactivate DocSearcher servlet/web support


## TODO for 5.0
- rewrite index management
- rewrite command line argument parsing


## TODO for next versions
- rework mnemonics!
- extra resources for translations and configs
- better reading of document metadata
- extend spider with option - spider only this url
- add warning for skipping files that are too large for indexing (add info in index result window)


## Bug fixing
- Prio high
  - remove temp files from TEMPDIR
  - create better temp file name (random)
- Prio low
  - replace email_from_address in resource with user definied address
  - try to fix spider index with domain without slash after tld (http://www.docsearcher.de)


## Questions?
Use the project forum https://sourceforge.net/p/docsearcher/discussion/
