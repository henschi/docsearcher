/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.filters;

import java.io.File;
import java.io.FilenameFilter;

import org.jab.docsearch.utils.FileUtils;

/**
 * Class FolderFilter
 */
public class FolderFilter implements FilenameFilter {
    /**
     * slash dot
     */
    private final static String slashDot = FileUtils.PATH_SEPARATOR + ".";


    /**
     * Overwrite FilenameFilter.accept(File, String)
     */
    @Override
	public boolean accept(File directory, String filename) {
        String newFolder = directory.toString() + FileUtils.PATH_SEPARATOR + filename;
        File testFile = new File(newFolder);
        if (testFile.isDirectory()) {
            return nonFpDir(newFolder);
        }
        return false;
    }


    /**
     * ???
     *
     * @param dirString
     * @return
     */
    public boolean nonFpDir(String dirString) {
        dirString = dirString.toLowerCase();

        if (dirString.contains("_vti")) {
            return false;
        }
        else if (dirString.contains("_derived")) {
            return false;
        }
        else if (dirString.contains("_private")) {
            return false;
        }
        else if (dirString.contains("_themes")) {
            return false;
        }
        else if (dirString.contains("_fpclass")) {
            return false;
        }
        else if (dirString.contains("_borders")) {
            return false;
        }
        else if (dirString.contains("cgi-bin")) {
            return false;
        }
        else if (dirString.contains("_overlay")) {
            return false;
        }
        else if (dirString.contains(slashDot)) {
            return false;
        }

        return true;
    }
}
