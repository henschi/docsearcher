/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.converters;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import org.apache.poi.hpsf.SummaryInformation;
import org.apache.poi.hpsf.extractor.HPSFPropertiesExtractor;
import org.apache.poi.hslf.usermodel.HSLFSlideShow;
import org.apache.poi.ooxml.POIXMLProperties.CoreProperties;
import org.apache.poi.openxml4j.opc.OPCPackage;
import org.apache.poi.poifs.filesystem.POIFSFileSystem;
import org.apache.poi.sl.extractor.SlideShowExtractor;
import org.apache.poi.sl.usermodel.SlideShow;
import org.apache.poi.sl.usermodel.SlideShowFactory;
import org.apache.poi.xslf.usermodel.XSLFSlideShow;
import org.jab.docsearch.utils.FileUtils;

/**
 * Class for handling MS PowerPoint files
 */
public class PowerPoint
        extends AbstractConverter
        implements ConverterInterface {

    private final String filename;


    /**
     * Constructor
     *
     * @param filename the PowerPoint file name
     */
    public PowerPoint(String filename) {
        this.filename = filename;
    }


    /**
     * @see ConverterInterface#parse()
     */
    @Override
    public void parse()
            throws ConverterException {
        if (filename == null) {
            log.error("parse() filename is null");
            throw new ConverterException("PowerPoint::parse() filename is null");
        }

        // check file filename
        String fileExt = FileUtils.getFileExtension(filename);

        // PowerPoint OOXML
        if ("pptx".equals(fileExt) || "ppsx".equals(fileExt)) {
            parseOOXML();
        }
        // PowerPoint OLE
        else {
            // get metadata and text
            try (POIFSFileSystem poiFS = new POIFSFileSystem(new File(filename));
                    SlideShow<?, ?> ss = SlideShowFactory.create(poiFS);
                    SlideShowExtractor<?, ?> sse = new SlideShowExtractor<>(ss);
                    HSLFSlideShow hss = new HSLFSlideShow(poiFS);
                    HPSFPropertiesExtractor hpe = hss.getMetadataTextExtractor()) {

                // get meta data
                final SummaryInformation si = hpe.getSummaryInformation();
                documentAuthor   = si.getAuthor();
                documentTitle    = si.getTitle();
                documentKeywords = si.getKeywords();

                // get text
                documentText = sse.getText();
            }
            catch (IOException ioe) {
                log.error("parse() failed at PowerPoint file={}", filename, ioe);
                throw new ConverterException("PowerPoint::parse() failed at PowerPoint file=" + filename, ioe);
            }
            catch (Exception e) {
                log.error("parse() failed at PowerPoint file={}", filename, e);
                throw new ConverterException("PowerPoint::parse() failed", e);
            }
        }

        log.debug("parse() PowerPoint file='{}'{}" +
                "title='{}'{}" +
                "author='{}'{}" +
                "keywords='{}'",
                filename, System.lineSeparator(), documentTitle, System.lineSeparator(),
                documentAuthor, System.lineSeparator(), documentKeywords);
    }


    /**
     * Parse file with PowerPoint OOXML Extractor
     *
     * @throws ConverterException  Converter problem
     */
    private void parseOOXML()
            throws ConverterException {

        // get metadata
        try (FileInputStream fin = new FileInputStream(filename);
                XSLFSlideShow xss = new XSLFSlideShow(OPCPackage.open(fin))) {

            // get meta data
            final CoreProperties cp = xss.getProperties().getCoreProperties();

            documentAuthor   = cp.getCreator();
            documentTitle    = cp.getTitle();
            documentKeywords = cp.getKeywords();
        }
        catch (IOException ioe) {
            log.error("parse() failed at PowerPoint file={}", filename, ioe);
            throw new ConverterException("PowerPoint::parseOOXML() failed at PowerPoint file=" + filename, ioe);
        }
        catch (Exception e) {
            log.error("parse() failed at PowerPoint file={}", filename, e);
            throw new ConverterException("PowerPoint::parseOOXML() failed", e);
        }

        // get text
        try (FileInputStream fin = new FileInputStream(filename);
                SlideShow<?, ?> ss = SlideShowFactory.create(fin);
                SlideShowExtractor<?, ?> sse = new SlideShowExtractor<>(ss)) {

            // get text
            documentText = sse.getText();
        }
        catch (IOException ioe) {
            log.error("parse() failed at PowerPoint file={}", filename, ioe);
            throw new ConverterException("PowerPoint::parseOOXML() failed at PowerPoint file=" + filename, ioe);
        }
        catch (Exception e) {
            log.error("parse() failed at PowerPoint file={}", filename, e);
            throw new ConverterException("PowerPoint::parseOOXML() failed", e);
        }
    }
}
