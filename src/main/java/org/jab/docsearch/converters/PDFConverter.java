/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.converters;

import java.io.File;
import java.io.IOException;
import java.io.StringWriter;

import org.apache.commons.lang3.StringUtils;
import org.apache.pdfbox.Loader;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDDocumentInformation;
import org.apache.pdfbox.pdmodel.encryption.AccessPermission;
import org.apache.pdfbox.text.PDFTextStripper;

/**
 * This class extract text of PDF document
 */
public class PDFConverter
        extends AbstractConverter
        implements ConverterInterface {

    private final String filename;


    /**
     * Constructor
     *
     * @param fileName  PDF file
     */
    public PDFConverter(String fileName) {
        this.filename = fileName;
    }


    /**
     * @see ConverterInterface#parse()
     */
    @Override
	public void parse()
            throws ConverterException {
        if (StringUtils.isBlank(filename)) {
            log.error("parse() filename is null");
            throw new ConverterException("PDFConverter::parse() filename is null");
        }

        // PD Document
        try (PDDocument document = Loader.loadPDF(new File(filename));
                StringWriter output = new StringWriter()) {

            // check document is readable
            final AccessPermission ap = document.getCurrentAccessPermission();
            if (! ap.canExtractContent()) {
                log.info("parse() can't extract content of PDF file");
                throw new ConverterException("PDFConverter::parse() can't extract content of PDF file");
            }

            // write text to buffer
            try {
                log.debug("parse() Attempting to extract text from ({})", filename);

                final PDFTextStripper stripper = new PDFTextStripper();
                stripper.writeText(document, output);

                log.debug("parse() Successfully stripped out text from ({})", filename);
            }
            catch (IOException ioe) {
                log.error("parse() failed", ioe);
                throw new ConverterException("PDFConverter::parse() failed", ioe);
            }

            // get the metadata
            final PDDocumentInformation info = document.getDocumentInformation();
            if (info != null) {
                documentTitle = info.getTitle();
                documentAuthor = info.getAuthor();
                documentKeywords = info.getKeywords();
            }
            documentText = output.toString();
        }
        catch (IOException ioe) {
            log.error("parse() failed", ioe);
            throw new ConverterException("parse() failed", ioe);
        }

        log.debug("parse() PDF file='{}'{}" +
                "title='{}'{}" +
                "author='{}'{}" +
                "keywords='{}'",
                filename, System.lineSeparator(), documentTitle, System.lineSeparator(),
                documentAuthor, System.lineSeparator(), documentKeywords);
    }
}
