/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.constants;

/**
 * This class contains all OS Type constants
 */
public final class OSType {
    /**
     * unknown
     */
    public static final int UNKNOWN = -1;
    /**
     * Windows
     */
    public static final int WINDOWS = 0;
    /**
     * Linux
     */
    public static final int LINUX = 1;
    /**
     * Unix
     */
    public static final int UNIX = 2;
    /**
     * MAC
     */
    public static final int MAC = 3;
}
