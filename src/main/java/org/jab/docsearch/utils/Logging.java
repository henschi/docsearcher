/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.utils;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.apache.logging.log4j.core.LoggerContext;
import org.apache.logging.log4j.core.appender.RollingFileAppender;
import org.apache.logging.log4j.core.appender.rolling.DefaultRolloverStrategy;
import org.apache.logging.log4j.core.appender.rolling.RolloverStrategy;
import org.apache.logging.log4j.core.appender.rolling.SizeBasedTriggeringPolicy;
import org.apache.logging.log4j.core.appender.rolling.TriggeringPolicy;
import org.apache.logging.log4j.core.config.Configuration;
import org.apache.logging.log4j.core.config.LoggerConfig;
import org.apache.logging.log4j.core.layout.PatternLayout;

/**
 * Class Logging
 */
public final class Logging {

    /**
     * Level NOTICE 
     */
    public static final Level NOTICE = Level.forName("NOTICE", Level.FATAL.intLevel() - 10);


    /**
     * Inits logging for DocSearcher
     */
    public void init() {
        // logger test
        final Logger logger = LogManager.getLogger(getClass().getName());
        logger.debug("init() logging is initialized");
    }


    /**
     * add logfile appender from Log4J
     */
    public void addFileLogger() {
        final Logger logger = LogManager.getLogger(getClass().getName());
        logger.info("addFileLogger() add logfile Appender");

        // get context and configuration
        final LoggerContext ctx = (LoggerContext)LogManager.getContext(false);
        final Configuration conf = ctx.getConfiguration();

        // create logging components
        final PatternLayout layout = PatternLayout.newBuilder().withPattern("%d %-6p [%c] %m%n").build();
        final RolloverStrategy strategy = DefaultRolloverStrategy.newBuilder().withMax("5").build();
        final TriggeringPolicy policy = SizeBasedTriggeringPolicy.createPolicy("5M");
        final RollingFileAppender logfileAppender = RollingFileAppender.newBuilder()
                .setName("logfile")
                .setLayout(layout)
                .withFileName("docsearcher.log")
                .withFilePattern("docsearcher-%i.log")
                .withStrategy(strategy)
                .withPolicy(policy)
                .setImmediateFlush(true)
                .build();

        // add appender and create logger
        if (logfileAppender != null) {
            logfileAppender.start();
            conf.addAppender(logfileAppender);

            // do all for logger
            final LoggerConfig loggerConfig = new LoggerConfig("org.jab.docsearch", Level.DEBUG, false);
            loggerConfig.addAppender(logfileAppender, null, null);
            conf.addLogger("org.jab.docsearch", loggerConfig);
            ctx.updateLoggers();
        }

        // check
        if (conf.getAppender("logfile") != null) {
            logger.info("addFileLogger() logfile logger successfully added");
        }
        else {
            logger.error("addFileLogger() logfile logger not successfully added");
        }
    }
}
