/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.utils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.file.FileSystems;

import org.apache.commons.codec.digest.DigestUtils;
import org.apache.commons.io.IOUtils;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * This class contains useful methods for handling files.
 */
public final class FileUtils {
    /**
     * Logging
     */
    private static final Logger logger = LogManager.getLogger(FileUtils.class);
    /**
     * Path separator
     */
    public static final String PATH_SEPARATOR = FileSystems.getDefault().getSeparator();


    /**
     * Checks file exist and is not a directory
     *
     * @param filename  file to check
     * @return          true if the file exists
     */
    public static boolean fileExists(final String filename) {
        if (filename == null) {
            logger.warn("fileExist() failed because filename is null");
            return false;
        }

        File file = new File(filename);

        // check file exist
        if (file.exists()) {
            // check file is directory
            return ! file.isDirectory();
        }
        else {
            return false;
        }
    }


    /**
     * Checks file exist and is not a directory
     *
     * @param filename  file to check
     * @return          true if the file exists
     */
    public static boolean deleteFile(final String filename) {
        if (filename == null) {
            logger.warn("deleteFile() failed because filename is null");
            return false;
        }

        File file = new File(filename);

        // check file exist
        if (file.exists() && ! file.isDirectory()) {
            return file.delete();
        }
        else {
            return false;
        }
    }


    /**
     * Gets file size
     *
     * @param filename  file name
     * @return          number of bytes in a file
     */
    public static long getFileSize(String filename) {
        if (! fileExists(filename)) {
            logger.warn("getFileSize() {} is not a file", filename);
            return 0;
        }

        try {
            return new File(filename).length();
        }
        catch (SecurityException se) {
            logger.fatal("getFileSize() failed", se);
            return 0;
        }
    }


    /**
     * Copy file
     *
     * @param sourceFilename       source file
     * @param destinationFilename  destination file
     */
    public static boolean copyFile(final String sourceFilename, final String destinationFilename) {
        if (sourceFilename == null) {
            logger.warn("copyFile() failed because sourceFilename is null");
            return false;
        }
        if (destinationFilename == null) {
            logger.warn("copyFile() failed because destinationFilename is null");
            return false;
        }

        // copy
        try (FileInputStream fis = new FileInputStream(sourceFilename);
                FileOutputStream fos = new FileOutputStream(destinationFilename)) {
            IOUtils.copy(fis, fos);
            return true;
        }
        catch (IOException ioe) {
            logger.fatal("copyFile() failed", ioe);
            return false;
        }
        catch (SecurityException se) {
            logger.fatal("copyFile() failed", se);
            return false;
        }
    }


    /**
     * Add folder to path
     *
     * @param startFolder  start folder
     * @param addedObject  object for adding
     * @return             start folder + add object
     */
    public static String addFolder(String startFolder, String addedObject) {
        if (startFolder == null) {
            logger.warn("addFolder() startFolder is null");
            startFolder = "";
        }
        if (addedObject == null) {
            logger.warn("addFolder() addedObject is null");
            addedObject = "";
        }

        // check if PATH_SEPARATOR is already at the end of start folder
        if (! startFolder.endsWith(PATH_SEPARATOR)) {
            return startFolder + PATH_SEPARATOR + addedObject;
        }
        else {
            return startFolder + addedObject;
        }
    }


    /**
     * Saves StringBuffer to file
     *
     * @param filename  file name
     * @param content   content for file
     * @return          true save was ok
     */
    public static boolean saveFile(final String filename, final StringBuffer content) {

        try (FileWriter  writer = new FileWriter(filename)) {
            writer.write(content.toString());
            return true;
        }
        catch (IOException ioe) {
            logger.fatal("saveFile() failed", ioe);
            return false;
        }
    }


    /**
     * Saves StringBuffer to file
     *
     * @param filename  file name
     * @param savePath  path
     * @param content   content for file
     * @return          true save was ok
     */
    public static boolean saveFile(String filename, String savePath, StringBuffer content) {

        String completeFileName = null;

        // add file to path
        completeFileName = addFolder(savePath, filename);

        // save content to file
        return saveFile(completeFileName, content);
    }


    /**
     * Gets MD5Sum for file
     *
     * @param file  file
     * @return      MD5Sum for a file
     */
    public static String getMD5Sum(final String file) {

        try (FileInputStream fin = new FileInputStream(file)) {
            return DigestUtils.md5Hex(fin);
        }
        catch (IOException ioe) {
            logger.fatal("getMD5Sum() failed for file='{}'", file, ioe);
            return "";
        }
    }


    /**
     * The extension of a filename - that indicates what kind of file it is.
     *
     * @param filename  filename
     * @return          last extension without point
     */
    public static String getFileExtension(String filename) {
        int lastPoint = filename.lastIndexOf(".");

        // only an extension exist
        if (lastPoint != -1 && lastPoint + 1 != filename.length()) {
            return filename.substring(lastPoint + 1);
        }
        else {
            return "unknown";
        }
    }
}
