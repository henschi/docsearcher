/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the
 * Free Software Foundation, Inc.,
 * 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA.
 */
package org.jab.docsearch.utils;

import java.io.File;
import java.io.IOException;

import org.apache.lucene.analysis.Analyzer;
import org.apache.lucene.analysis.standard.StandardAnalyzer;
import org.apache.lucene.document.FieldType;
import org.apache.lucene.index.DirectoryReader;
import org.apache.lucene.index.IndexOptions;
import org.apache.lucene.index.IndexReader;
import org.apache.lucene.index.IndexWriter;
import org.apache.lucene.index.IndexWriterConfig;
import org.apache.lucene.index.IndexWriterConfig.OpenMode;
import org.apache.lucene.queryparser.classic.QueryParser;
import org.apache.lucene.store.Directory;
import org.apache.lucene.store.NIOFSDirectory;
import org.apache.lucene.util.Bits;

/**
 * This class contains useful methods for working with Lucene.
 */
public class LuceneUtils {

    /**
     * Gets the Lucene index directory.
     *
     * @param path  Path of Lucene index directory
     * @return Lucene Directory
     * @throws IOException
     */
    public static Directory getDirectory(final String path) throws IOException {
        return getDirectory(new File(path));
    }


    /**
     * Gets the Lucene index directory.
     *
     * @param path  Path of Lucene index directory
     * @return Lucene Directory
     * @throws IOException
     */
    public static Directory getDirectory(final File path) throws IOException {
        return new NIOFSDirectory(path.toPath());
    }


    /**
     * Gets the Lucene analyzer.
     *
     * @return Lucene Analyzer
     */
    private static Analyzer getAnalyzer() {
        return new StandardAnalyzer();
    }


    /**
     * Gets the Lucene query parser.
     *
     * @param defaultSearchField  The default search field name
     * @return  Lucene query parser
     */
    public static QueryParser getQueryParser(final String defaultSearchField) {
        return new QueryParser(defaultSearchField, getAnalyzer());
    }


    /**
     * Get the Lucene index writer.
     *
     * @param directory  Lucene index directory
     * @param create     Create a new index
     * @return Lucene index writer
     * @throws IOException
     */
    public static IndexWriter getIndexWriter(final Directory directory, final boolean create) throws IOException {
    
        // index writer config
        IndexWriterConfig iwc = new IndexWriterConfig(getAnalyzer());

        // FIXME Is this really necessary, or the default (create_and_append) enough?
        // new or existing index
        if (create) {
            iwc.setOpenMode(OpenMode.CREATE);
        }
        else {
            iwc.setOpenMode(OpenMode.APPEND);
        }

        // index writer
        IndexWriter iw = new IndexWriter(directory, iwc);

        // ready
        return iw;
    }


    /**
     * Get the Lucene index reader.
     * 
     * @param directory  Lucene index directory
     * @return Lucene index reader.
     * @throws IOException
     */
    public static IndexReader getIndexReader(final Directory directory) throws IOException {
        return DirectoryReader.open(directory);
    }


    /**
     * Checks document is delete.
     * 
     * FIXME Check for too frequent creation of livedocs.
     * 
     * @param liveDocs  Live documents of index.
     * @param docID     Document ID
     * @return          true if document is deleted
     */
    public static boolean isDocumentDeleted(final Bits liveDocs, int docID) {
    
        // docID not exist in liveFocs
        if (liveDocs != null && !liveDocs.get(docID)) {
            return true;
        }
        return false;
    }


    /**
     * Get FieldType analog to Lucene 3.6 Field.Store.YES, Field.Index.NO
     * 
     * @return The FieldType
     */
    public static FieldType getFieldTypeStoreIndexNo() {
        final FieldType fType = new FieldType(); 
        fType.setStored(true);
        fType.setIndexOptions(IndexOptions.NONE);
        fType.setTokenized(false);
        return fType;
    }


    /**
     * Get FieldType analog to Lucene 3.6 Field.Store.YES, Field.Index.ANALYZED
     * 
     * @return The FieldType
     */
    public static FieldType getFieldTypeStoreIndexAnalyze() {
        final FieldType fType = new FieldType(); 
        fType.setStored(true);
        fType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        fType.setTokenized(true);
        return fType;
    }


    /**
     * Get FieldType analog to Lucene 3.6 Field.Store.YES, Field.Index.NOT_ANALYZED
     * 
     * @return The FieldType
     */
    public static FieldType getFieldTypeStoreIndexAnalyzeNo() {
        final FieldType fType = new FieldType(); 
        fType.setStored(true);
        fType.setIndexOptions(IndexOptions.DOCS);
        fType.setTokenized(false);
        return fType;
    }


    /**
     * Get FieldType analog to Lucene 3.6 Field.Store.NO, Field.Index.ANALYZED
     * 
     * @return The FieldType
     */
    public static FieldType getFieldTypeStoreNoIndexAnalyze() {
        final FieldType fType = new FieldType(); 
        fType.setStored(false);
        fType.setIndexOptions(IndexOptions.DOCS_AND_FREQS_AND_POSITIONS);
        fType.setTokenized(true);
        return fType;
    }
}
