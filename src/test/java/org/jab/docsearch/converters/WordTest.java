package org.jab.docsearch.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at Word
 */
public class WordTest {

    private Word converter;
    private static String doubleLineSep = System.lineSeparator() + System.lineSeparator();


    @BeforeAll
    public static void setUpClass() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.WARN);
    }


    @Test
    void testParseWithProblem() {

        // null
        converter = new Word(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // unknown file
        converter = new Word("foo.doc");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParseWord6_oo3()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/word-6-oo3.doc").getFile();

        // parse
        converter = new Word(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content." + doubleLineSep, converter.getSummary());
        assertEquals("This is test content." + doubleLineSep, converter.getText());
    }


    @Test
    void testParseWord8_oo3()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/word-8-oo3.doc").getFile();

        // parse
        converter = new Word(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content." + doubleLineSep, converter.getSummary());
        assertEquals("This is test content." + doubleLineSep, converter.getText());
    }


    @Test
    void testParseWord8_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/word-8-o2007.doc").getFile();

        // parse
        converter = new Word(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content." + doubleLineSep, converter.getSummary());
        assertEquals("This is test content." + doubleLineSep, converter.getText());
    }


    @Test
    void testParseWord8_o2007_pwd() {

        // file lookup in classpath
        String file = getClass().getResource("/word-8-o2007-pwd.doc").getFile();

        // parse
        converter = new Word(file);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParseWordOOXML_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/word-ooxml-o2007.docx").getFile();

        // parse
        converter = new Word(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content.\n", converter.getSummary());
        assertEquals("This is test content.\n", converter.getText());
    }
}
