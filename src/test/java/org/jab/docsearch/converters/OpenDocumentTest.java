package org.jab.docsearch.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at OpenDocument
 */
public class OpenDocumentTest {

    private OpenDocument converter;


    @BeforeAll
    public static void setUpClass() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.WARN);
    }


    @Test
    void testParseWithProblem() {

        // null
        converter = new OpenDocument(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // unknown file
        converter = new OpenDocument("foo.odt");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParseODT()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/opendocument.odt").getFile();

        // parse
        converter = new OpenDocument(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content. ", converter.getSummary());
        assertEquals("This is test content. ", converter.getText());
    }


    @Test
    void testParseODS()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/opendocument.ods").getFile();

        // parse
        converter = new OpenDocument(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content. ", converter.getSummary());
        assertEquals("This is test content. ", converter.getText());
    }


    @Test
    void testParseODP()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/opendocument.odp").getFile();

        // parse
        converter = new OpenDocument(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content. ", converter.getSummary());
        assertEquals("This is test content. ", converter.getText());
    }


    @Test
    void testParseODG()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/opendocument.odg").getFile();

        // parse
        converter = new OpenDocument(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content. ", converter.getSummary());
        assertEquals("This is test content. ", converter.getText());
    }
}
