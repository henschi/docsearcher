package org.jab.docsearch.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at Excel
 */
public class ExcelTest {

    private Excel converter;


    @BeforeAll
    public static void setUpClass() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.WARN);
    }


    @Test
    void testParseWithProblem() {

        // null
        converter = new Excel(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // unknown file
        converter = new Excel("foo.xls");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParseExcel5_oo3() {

        // file lookup in classpath
        String file = getClass().getResource("/excel-5-oo3.xls").getFile();

        // parse
        converter = new Excel(file);
        try {
            converter.parse();
            fail("ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    public void testParseExcel8_oo3()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/excel-8-oo3.xls").getFile();

        // parse
        converter = new Excel(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content.\n", converter.getSummary());
        assertEquals("This is test content.\n", converter.getText());
    }


    @Test
    void testParseExcel8_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/excel-8-o2007.xls").getFile();

        // parse
        converter = new Excel(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content.\n", converter.getSummary());
        assertEquals("This is test content.\n", converter.getText());
    }


    @Test
    void testParseExcel8_o2007_pwd() {

        // file lookup in classpath
        String file = getClass().getResource("/excel-8-o2007-pwd.xls").getFile();

        // parse
        converter = new Excel(file);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParseExcelOOXML_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/excel-ooxml-o2007.xlsx").getFile();

        // parse
        converter = new Excel(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test content.\n", converter.getSummary());
        assertEquals("This is test content.\n", converter.getText());
    }
}
