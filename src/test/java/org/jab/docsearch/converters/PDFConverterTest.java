package org.jab.docsearch.converters;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.fail;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.jab.docsearch.utils.FileUtils;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at PDFConverter
 */
public class PDFConverterTest {

    private PDFConverter converter;


    @BeforeAll
    public static void setUpClass() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.WARN);
    }


    @Test
    void testParseWithProblem() {

        // filename null
        converter = new PDFConverter(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }

        // unknown file
        converter = new PDFConverter("foo.pdf");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }
    }


    @Test
    void testParsePDF_1_4()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/pdf-1.4.pdf").getFile();

        // parse
        converter = new PDFConverter(file);
        converter.parse();

        // check content
        assertEquals("Testdocument", converter.getTitle());
        assertEquals("Torsten Henschel", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        if ("/".equals(FileUtils.PATH_SEPARATOR)) {
            assertEquals("This is test content.\n", converter.getSummary());
            assertEquals("This is test content.\n", converter.getText());
        }
        else {
            assertEquals("This is test content.\r\n", converter.getSummary());
            assertEquals("This is test content.\r\n", converter.getText());
        }
    }
}
