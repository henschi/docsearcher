package org.jab.docsearch.converters;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.core.config.Configurator;
import org.apache.logging.log4j.core.config.DefaultConfiguration;
import org.apache.poi.hslf.exceptions.OldPowerPointFormatException;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

/**
 * Test class at PowerPoint
 */
public class PowerPointTest {

    private PowerPoint converter;


    @BeforeAll
    public static void setUpClass() {
        Configurator.initialize(new DefaultConfiguration());
        Configurator.setRootLevel(Level.WARN);
    }


    @Test
    void testParseWithProblem() {

        // null
        converter = new PowerPoint(null);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // unknown file
        converter = new PowerPoint("foo.ppt");
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // ok
        }


        // PowerPoint5 file
        String file = getClass().getResource("/powerpoint-5-oxp.ppt").getFile();
        converter = new PowerPoint(file);
        try {
            converter.parse();
            fail("fail, because ConverterException expected");
        }
        catch (ConverterException ce) {
            // check for OldPowerPointFormatException
            assertInstanceOf(OldPowerPointFormatException.class, ce.getCause());
            //ce.printStackTrace();
        }
    }


    @Test
    void testParsePowerpoint7_PPT_oxp()
            throws ConverterException {
        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-oxp.ppt").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title ", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals(null, converter.getKeywords());
        assertEquals("This is test title \n\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("This is test title \n\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    void testParsePowerpoint7_PPS_oxp()
            throws ConverterException {
        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-oxp.pps").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title ", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals(null, converter.getKeywords());
        assertEquals("This is test title \n\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("This is test title \n\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    void testParsePowerpoint7_PPT_o2010()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-o2010.ppt").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    void testParsePowerpoint7_PPS_o2010()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-7-o2010.pps").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals("JUnit", converter.getKeywords());
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getSummary());
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getText());
    }


    @Test
    void testParsePowerpointOOXML_PPT_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-ooxml-o2007.pptx").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title", converter.getTitle());
        assertEquals("Olivier Descout", converter.getAuthor());
        assertEquals(null, converter.getKeywords());
        assertEquals("This is test title\n\nTest\nThis is test content.\nThat is second test content.\n", converter.getSummary());
        assertEquals("This is test title\n\nTest\nThis is test content.\nThat is second test content.\n", converter.getText());
    }


    @Test
    void testParsePowerpointOOXML_PPS_o2007()
            throws ConverterException {

        // file lookup in classpath
        String file = getClass().getResource("/powerpoint-ooxml-o2007.ppsx").getFile();

        // parse
        converter = new PowerPoint(file);
        converter.parse();

        // check content
        assertEquals("This is test title", converter.getTitle(), "Title");
        assertEquals("Olivier Descout", converter.getAuthor(), "Author");
        assertEquals(null, converter.getKeywords(), "Keywords");
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getSummary(), "Summary");
        assertEquals("This is test title\n\nTest\nThis is test content.\n", converter.getText(), "Text");
    }
}
