package org.jab.docsearch.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Locale;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at Utils
 */
public class UtilsTest {

    private static Locale defaultLocale;

    /**
     * Before all tests
     */
    @BeforeAll
    public static void beforeAll() {
        defaultLocale = Locale.getDefault();
    }


    /**
     * After all tests
     */
    @AfterAll
    public static void afterAll() {
        Locale.setDefault(defaultLocale);
    }


    @Test
    public void testReplaceAll() {
        // once
        assertEquals("&amp;", Utils.replaceAll("&", "&", "&amp;"));
        assertEquals("&", Utils.replaceAll("&amp;", "&amp;", "&"));

        // begin
        assertEquals("&amp;--------", Utils.replaceAll("&", "&--------", "&amp;"));
        assertEquals("&--------", Utils.replaceAll("&amp;", "&amp;--------", "&"));

        // middle
        assertEquals("----&amp;----", Utils.replaceAll("&", "----&----", "&amp;"));
        assertEquals("----&----", Utils.replaceAll("&amp;", "----&amp;----", "&"));

        // end
        assertEquals("--------&amp;", Utils.replaceAll("&", "--------&", "&amp;"));
        assertEquals("--------&", Utils.replaceAll("&amp;", "--------&amp;", "&"));
    }


    @Test
    public void testConvertTextToHTML() {
        assertEquals(null, Utils.convertTextToHTML(null));
        assertEquals("&amp;&amp;", Utils.convertTextToHTML("&&"));
        assertEquals("&nbsp;", Utils.convertTextToHTML("\n"));
        assertEquals("&lt;", Utils.convertTextToHTML("<"));
        assertEquals("&gt;", Utils.convertTextToHTML(">"));
        assertEquals("&quot;", Utils.convertTextToHTML("\""));
    }


    @Test
    public void testGetBaseURLFolder() {
        assertEquals(null, Utils.getBaseURLFolder(null));
        assertEquals(" ", Utils.getBaseURLFolder(" "));
        assertEquals("Hello", Utils.getBaseURLFolder("Hello"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/index.html"));
        assertEquals("http://www.docsearcher.de/", Utils.getBaseURLFolder("http://www.docsearcher.de/test"));
        assertEquals("http://www.docsearcher.de/test/", Utils.getBaseURLFolder("http://www.docsearcher.de/test/"));
        assertEquals("http://www.docsearcher.de/test/", Utils.getBaseURLFolder("http://www.docsearcher.de/test/index.html"));
    }


    @Test
    public void testGetDomainURL() {
        assertEquals("", Utils.getDomainURL(null));
        assertEquals("", Utils.getDomainURL(""));
        assertEquals("", Utils.getDomainURL("Hello"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/index.html"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test/"));
        assertEquals("http://www.docsearcher.de/", Utils.getDomainURL("http://www.docsearcher.de/test/index.html"));
    }


    @Test
    public void testGetByteString() {

        // en_US
        Locale.setDefault(Locale.US);
        assertEquals("1 kB", Utils.getByteString("1024"));
        assertEquals("1.44 kB", Utils.getByteString("1470"));
        assertEquals("10 kB", Utils.getByteString("10240"));
        assertEquals("14.36 kB", Utils.getByteString("14704"));
        assertEquals("0.17 MB", Utils.getByteString("174080"));
        assertEquals("1.17 MB", Utils.getByteString("1222656"));

        // de_DE
        Locale.setDefault(Locale.GERMANY);
        assertEquals("1 kB", Utils.getByteString("1024"));
        assertEquals("1,44 kB", Utils.getByteString("1470"));
        assertEquals("10 kB", Utils.getByteString("10240"));
        assertEquals("14,36 kB", Utils.getByteString("14704"));
        assertEquals("0,17 MB", Utils.getByteString("174080"));
        assertEquals("1,17 MB", Utils.getByteString("1222656"));

        // fr_FR
        Locale.setDefault(Locale.FRANCE);
        assertEquals("1 kB", Utils.getByteString("1024"));
        assertEquals("1,44 kB", Utils.getByteString("1470"));
        assertEquals("10 kB", Utils.getByteString("10240"));
        assertEquals("14,36 kB", Utils.getByteString("14704"));
        assertEquals("0,17 MB", Utils.getByteString("174080"));
        assertEquals("1,17 MB", Utils.getByteString("1222656"));
    }
}
