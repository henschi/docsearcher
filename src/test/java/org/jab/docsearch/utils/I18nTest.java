package org.jab.docsearch.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.Date;
import java.util.Locale;

import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;

/**
 * Test class at I18n
 */
public class I18nTest {

    private static Locale defaultLocale;

    /**
     * Before all tests
     */
    @BeforeAll
    public static void beforeAll() {
        defaultLocale = Locale.getDefault();
    }


    /**
     * After all tests
     */
    @AfterAll
    public static void afterAll() {
        Locale.setDefault(defaultLocale);
    }


    /**
     * Test formatToString
     */
    @Test
    public void testFormatToString() {

        // en_US
        Locale.setDefault(Locale.US);
        assertEquals("15678.12", I18n.formatToString(I18n.DECIMAL_FORMAT, 15678.1234));
        // de_DE
        Locale.setDefault(Locale.GERMANY);
        assertEquals("15678,12", I18n.formatToString(I18n.DECIMAL_FORMAT, 15678.1234));
        // fr_FR
        Locale.setDefault(Locale.FRANCE);
        assertEquals("15678,12", I18n.formatToString(I18n.DECIMAL_FORMAT, 15678.1234));
    }

    /**
     * Test formatDateToString
     */
    @Test
    public void testFormatDateToString() {

        // en_US
        Locale.setDefault(Locale.US);
        assertEquals("Jan 1, 1970", I18n.formatDateToString(new Date(1)));
        // de_DE
        Locale.setDefault(Locale.GERMANY);
        assertEquals("01.01.1970", I18n.formatDateToString(new Date(1)));
        // fr_FR
        Locale.setDefault(Locale.FRANCE);
        assertEquals("1 janv. 1970", I18n.formatDateToString(new Date(1)));
    }
}
