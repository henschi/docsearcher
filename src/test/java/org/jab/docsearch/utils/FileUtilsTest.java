package org.jab.docsearch.utils;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class at FileUtils
 */
public class FileUtilsTest {

    @Test
    public void testGetFileExtension() {
        // null
        //assertEquals(null, fileUtils.getFileExtension(null));

        // empty
        assertEquals("unknown", FileUtils.getFileExtension(""));
        assertEquals("unknown", FileUtils.getFileExtension(" "));

        // without extension
        assertEquals("unknown", FileUtils.getFileExtension("filename"));
        assertEquals("unknown", FileUtils.getFileExtension("filename."));
        assertEquals("unknown", FileUtils.getFileExtension("filename.."));

        // with one extension
        assertEquals("extension", FileUtils.getFileExtension("filename.extension"));

        // with more than one extension
        assertEquals("extension2", FileUtils.getFileExtension("filename.extension1.extension2"));
    }


    @Test
    public void testGetMD5Sum() {
        final String md5Sum = FileUtils.getMD5Sum(getClass().getResource("/md5sum-test.txt").getFile());
        assertEquals("7dd4bd30f5a0830c7be516815dea18bd", md5Sum);
    }
}
