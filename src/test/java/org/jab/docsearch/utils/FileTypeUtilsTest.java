package org.jab.docsearch.utils;

import static org.junit.jupiter.api.Assertions.assertFalse;
import static org.junit.jupiter.api.Assertions.assertTrue;

import org.junit.jupiter.api.Test;

/**
 * Test class at FileTypeUtils
 */
public class FileTypeUtilsTest {

    @Test
    public void testIsFileTypeText() {
        // not text
        assertFalse(FileTypeUtils.isFileTypeText(null));
        assertFalse(FileTypeUtils.isFileTypeText("filename"));
        assertFalse(FileTypeUtils.isFileTypeText("filename.abc"));

        // text
        assertTrue(FileTypeUtils.isFileTypeText("filename.txt"));
        assertTrue(FileTypeUtils.isFileTypeText("FILENAME.TXT"));
    }


    @Test
    public void testIsFileTypeHTML() {
        // not html
        assertFalse(FileTypeUtils.isFileTypeHTML(null));
        assertFalse(FileTypeUtils.isFileTypeHTML("filename"));
        assertFalse(FileTypeUtils.isFileTypeHTML("filename.abc"));

        // html
        assertTrue(FileTypeUtils.isFileTypeHTML("filename.html"));
        assertTrue(FileTypeUtils.isFileTypeHTML("FILENAME.HTML"));
    }
}
