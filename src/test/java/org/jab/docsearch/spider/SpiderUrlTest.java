package org.jab.docsearch.spider;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class at SpiderUrl
 */
public class SpiderUrlTest {

    @Test
    public void testConstructor() {
        SpiderUrl su = new SpiderUrl("url|12345|67890|type|md5");
        assertEquals("url", su.getUrl());
        assertEquals(12345, su.getLastModified());
        assertEquals(67890, su.getSize());
        assertEquals("type", su.getContentType());
        assertEquals("md5", su.getMd5());
    }
}
