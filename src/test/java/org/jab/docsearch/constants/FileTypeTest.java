package org.jab.docsearch.constants;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

/**
 * Test class at FileType
 */
public class FileTypeTest {

	/**
	 * Test fromValue method
	 */
	@Test
	void testFromValue() {

		// unknown
		assertEquals(FileType.UNKNOWN, FileType.fromValue(null));
		assertEquals(FileType.UNKNOWN, FileType.fromValue(""));
		assertEquals(FileType.UNKNOWN, FileType.fromValue("fromValue"));

		// known
		assertEquals(FileType.HTML, FileType.fromValue("html"));
		assertEquals(FileType.HTML, FileType.fromValue("HTML"));
	}
}
